package com.appsdeveloperblog.estore.AccountsService.command.rest;


//import com.appsdeveloperblog.estore.AccountsService.core.data.DeleteAxonTableRowsService;
import com.appsdeveloperblog.estore.AccountsService.command.BlockAccountCommand;
import com.appsdeveloperblog.estore.AccountsService.core.data.AccountLookupRepository;
import com.appsdeveloperblog.estore.AccountsService.core.data.AccountsRepository;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/clean-accounts-axon")
public class ClearAxonTablesController {

   // @Autowired
  //  private DeleteAxonTableRowsService deleteAxonTableRowsService;
    private AccountsRepository accountsRepository;

  //  @Autowired
    private AccountLookupRepository accountLookupRepository;

    private final CommandGateway commandGateway;

    @Autowired
    public ClearAxonTablesController(Environment env,
                                     CommandGateway commandGateway,
                                     AccountsRepository accountsRepository,
                                     AccountLookupRepository accountLookupRepository) {

        this.commandGateway = commandGateway;
        this.accountsRepository  = accountsRepository;
        this.accountLookupRepository = accountLookupRepository;
    }

    @DeleteMapping
    public ResponseEntity<String> createProduct(){

        try {

            accountsRepository.deleteAll();
            accountLookupRepository.deleteAll();
            return new ResponseEntity<>(
                    "Aux Accounts Axon tables cleaned",
                    HttpStatus.OK);
        }
        catch(Exception e){
            return new ResponseEntity<>(
                    e.getMessage(),
                    HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PostMapping(value = "/block")
    public ResponseEntity<String> blockAccount(@RequestBody BlockAccountRestModel order){
        BlockAccountCommand blockAccountCommand = BlockAccountCommand.builder()
                .accountId(order.getAccountId())
                .build();

        try{
          //  LOGGER.info("/blocked output: ",result);
            String returnValue = commandGateway.sendAndWait(blockAccountCommand);

            return new ResponseEntity<>(
                    "Account "+order.getAccountId() +"  "+ returnValue + " is blocked! Null means blocked successfully!",
                    HttpStatus.OK);
        }
        catch (Exception e){
         //   LOGGER.info("/blocked output exception: ",e.getMessage());
            return new ResponseEntity<>(
                    e.getMessage(),
                    HttpStatus.EXPECTATION_FAILED);
        }
    }
//    @PostMapping(value = "/unblock")
//    public ResponseEntity<String> unblockAccount(@RequestBody TransactionCreateRest order){
//        try{
//            neoService.unblockAccount(order.getProductFromId());
//            return new ResponseEntity<>(
//                    "Account "+ order.getProductFromId() + " is unblocked!",
//                    HttpStatus.OK);
//        }
//        catch (Exception e){
//            return new ResponseEntity<>(
//                    e.getMessage(),
//                    HttpStatus.EXPECTATION_FAILED);
//        }
//    }
}
