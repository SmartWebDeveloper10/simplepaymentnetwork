package com.appsdeveloperblog.estore.AccountsService.command.interceptors;

import java.util.List;
import java.util.function.BiFunction;

import com.appsdeveloperblog.estore.AccountsService.core.data.AccountLookupRepository;
import org.axonframework.commandhandling.CommandMessage;
import org.axonframework.messaging.MessageDispatchInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class CreateAccountCommandInterceptor implements MessageDispatchInterceptor<CommandMessage<?>> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CreateAccountCommandInterceptor.class);
	private final AccountLookupRepository accountLookupRepository;
	
	public CreateAccountCommandInterceptor(AccountLookupRepository accountLookupRepository) {
		this.accountLookupRepository = accountLookupRepository;
	}
 
	@Override
	public BiFunction<Integer, CommandMessage<?>, CommandMessage<?>> handle(
			List<? extends CommandMessage<?>> messages) {
		 
		return (index, command) -> {
			
//			LOGGER.info("Intercepted command: " + command.getPayloadType());
//
//			if(CreateAccountCommand.class.equals(command.getPayloadType())) {
//
//				CreateAccountCommand createProductCommand = (CreateAccountCommand)command.getPayload();
//
//				AccountLookupEntity productLookupEntity =  accountLookupRepository.findByProductIdOrTitle(createProductCommand.getProductId(),
//						createProductCommand.getTitle());
//
//				if(productLookupEntity != null) {
//					throw new IllegalStateException(
//							String.format("Product with productId %s or title %s already exist",
//									createProductCommand.getProductId(), createProductCommand.getTitle())
//							);
//				}
//
//			}
			
			return command;
		};
	}

}
