package com.appsdeveloperblog.estore.AccountsService.query.rest;

import java.util.List;

import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.appsdeveloperblog.estore.AccountsService.query.FindAccountsQuery;

@RestController
@RequestMapping("/products")
public class AccountsQueryController {
	
	@Autowired
	QueryGateway queryGateway;
	
	@GetMapping
	public List<AccountRestModel> getProducts() {
		
		FindAccountsQuery findAccountsQuery = new FindAccountsQuery();
		List<AccountRestModel> accounts = queryGateway.query(findAccountsQuery,
				ResponseTypes.multipleInstancesOf(AccountRestModel.class)).join();
		
		return accounts;
		
		
	}

}
