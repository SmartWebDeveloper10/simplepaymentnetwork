package com.appsdeveloperblog.estore.AccountsService.core.data;

import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface AccountNodeRepository extends Neo4jRepository<AccountNode, String> {
}
