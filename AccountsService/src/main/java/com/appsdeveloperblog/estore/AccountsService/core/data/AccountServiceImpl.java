package com.appsdeveloperblog.estore.AccountsService.core.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService{

    @Autowired
    private AccountsRepository accountsRepository;

    @Override
    public BigDecimal getBalanceById(String accountId) {
        Optional<AccountEntity> account = accountsRepository.findById(accountId);
        if(account.isPresent()){
            return account.get().getBalance();
        }
        else{
            return null;
        }


    }
}
