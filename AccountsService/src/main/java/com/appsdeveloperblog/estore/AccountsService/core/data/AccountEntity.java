package com.appsdeveloperblog.estore.AccountsService.core.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;


@Entity
@Table(name="accounts")
@Data
public class AccountEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -227264951080660124L;
	
	@Id
	@Column(unique=true)
	private String accountId;

	private BigDecimal balance;
	private Boolean isBlocked = false;

	@Column(name = "created", nullable = false)
	@CreationTimestamp
	private LocalDateTime created;


	@Column(name = "updated", nullable = false)
	@UpdateTimestamp
	private LocalDateTime updated;

}
