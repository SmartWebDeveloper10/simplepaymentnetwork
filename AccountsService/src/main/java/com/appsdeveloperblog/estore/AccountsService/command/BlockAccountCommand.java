package com.appsdeveloperblog.estore.AccountsService.command;


import lombok.Builder;
import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Builder
@Data
public class BlockAccountCommand {

    @TargetAggregateIdentifier
    private final String accountId;
}
