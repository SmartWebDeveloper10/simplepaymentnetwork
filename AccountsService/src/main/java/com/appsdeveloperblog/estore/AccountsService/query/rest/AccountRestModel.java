package com.appsdeveloperblog.estore.AccountsService.query.rest;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class AccountRestModel {
	private String accountId;
//	private String title;
//	private BigDecimal price;
	private BigDecimal amount;
}
