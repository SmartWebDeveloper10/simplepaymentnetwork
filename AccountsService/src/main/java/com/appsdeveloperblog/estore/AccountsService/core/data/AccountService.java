package com.appsdeveloperblog.estore.AccountsService.core.data;

import java.math.BigDecimal;
import java.util.Optional;

public interface AccountService {

    public BigDecimal getBalanceById(String accountId) ;
}
