package com.appsdeveloperblog.estore.AccountsService.core.data;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountLookupRepository extends JpaRepository<AccountLookupEntity, String> {
	AccountLookupEntity findByProductIdOrTitle(String productId, String title);
}
