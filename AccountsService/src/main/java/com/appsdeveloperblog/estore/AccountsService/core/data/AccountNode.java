package com.appsdeveloperblog.estore.AccountsService.core.data;




import lombok.Data;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;

@Data
@Node("Account")
public class AccountNode {

    @Id
    private String accountId;
    private boolean blocked;
}
