package com.appsdeveloperblog.estore.AccountsService.core.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface AccountsRepository extends JpaRepository<AccountEntity, String> {
	
	AccountEntity findByAccountId(String accountId);
//	AccountEntity findByProductIdOrTitle(String productId, String title);

	@Transactional
	@Modifying
	@Query(value =
			"DELETE * FROM ACCOUNTS;"
			, nativeQuery = true)
	void deleteAllAuxTables();

}
