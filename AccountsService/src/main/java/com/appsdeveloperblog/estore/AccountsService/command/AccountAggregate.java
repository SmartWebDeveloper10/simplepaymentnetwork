package com.appsdeveloperblog.estore.AccountsService.command;

import java.math.BigDecimal;

import com.appsdeveloperblog.estore.AccountsService.core.events.AccountBlockedEvent;
import com.appsdeveloperblog.estore.core.commands.DepositMoneyCommand;
import com.appsdeveloperblog.estore.core.events.MoneyDepositedEvent;
import com.appsdeveloperblog.estore.core.events.MoneyRolledbackEvent;
import com.appsdeveloperblog.estore.core.events.MoneyReservedEvent;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import com.appsdeveloperblog.estore.AccountsService.core.events.AccountCreatedEvent;
import com.appsdeveloperblog.estore.core.commands.MoneyRollbackCommand;
import com.appsdeveloperblog.estore.core.commands.ReserveMoneyCommand;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Aggregate(snapshotTriggerDefinition="productSnapshotTriggerDefinition")
public class AccountAggregate {
	private static final Logger LOGGER = LoggerFactory.getLogger(AccountAggregate.class);
	
	@AggregateIdentifier
	private String accountId;
	private BigDecimal balance;
	private Boolean isBlocked = false;
	
	public AccountAggregate() {
		
	}
	
	@CommandHandler
	public AccountAggregate(CreateAccountCommand createAccountCommand) {
		// Validate Create Product Command
		LOGGER.info("CreateAccountCommand in  productAggregateId: " + createAccountCommand.getAccountId());
		if(createAccountCommand.getBalance().compareTo(BigDecimal.ZERO) <= 0) {
			throw new IllegalArgumentException("Amount cannot be less or equal than zero");
		}
		
		AccountCreatedEvent accountCreatedEvent = new AccountCreatedEvent();
		
		BeanUtils.copyProperties(createAccountCommand, accountCreatedEvent);

		AggregateLifecycle.apply(accountCreatedEvent);
	}

	@CommandHandler
	public void handle(BlockAccountCommand blockAccountCommand){
		AccountBlockedEvent ev = new AccountBlockedEvent();
		ev.setAccountId(blockAccountCommand.getAccountId());
		AggregateLifecycle.apply(ev);
	}
	
	@CommandHandler
	public void handle(ReserveMoneyCommand reserveMoneyCommand) {
		LOGGER.info("ReserveMoneyCommand in  productAggregateId: " + reserveMoneyCommand.getAccountFromId());
		if(balance.compareTo( reserveMoneyCommand.getAmount())==-1) {
			LOGGER.info("Insufficient funds on the account "+this.accountId);
			throw new IllegalArgumentException("Insufficient funds on the account "+this.accountId);
		}
		if(isBlocked == true) {
			LOGGER.info("The from account is blocked: "+this.accountId);
			throw new IllegalArgumentException("The from account is blocked: "+this.accountId);
		}
		MoneyReservedEvent moneyReservedEvent = MoneyReservedEvent.builder()
				.transactionId(reserveMoneyCommand.getTransactionId())
				.accountFromId(reserveMoneyCommand.getAccountFromId())
				.accountToId(reserveMoneyCommand.getAccountToId())
				.amount(reserveMoneyCommand.getAmount())
				.build();

		AggregateLifecycle.apply(moneyReservedEvent);
		
	}

	@CommandHandler
	public void handle(DepositMoneyCommand depositMoneyCommand) {

		LOGGER.info("DepositMoneyCommand in  productAggregateId: " + depositMoneyCommand.getAccountToId());

		if(isBlocked == true) {
			throw new IllegalArgumentException("The to account is blocked: "+this.accountId);
		}

		MoneyDepositedEvent moneyDepositedEvent = MoneyDepositedEvent.builder()
				.transactionId(depositMoneyCommand.getTransactionId())
				.accountFromId(depositMoneyCommand.getAccountFromId())
				.accountToId(depositMoneyCommand.getAccountToId())
				.amount(depositMoneyCommand.getAmount())
				.build();

		AggregateLifecycle.apply(moneyDepositedEvent);

	}
	
	@CommandHandler
	public void handle(MoneyRollbackCommand moneyRollbackCommand) {
		
		MoneyRolledbackEvent moneyRolledbackEvent =
				MoneyRolledbackEvent.builder()
				.transactionId(moneyRollbackCommand.getTransactionId())
				.accountId(moneyRollbackCommand.getAccountId())
				.amount(moneyRollbackCommand.getAmount())
				.reason(moneyRollbackCommand.getReason())
				.build();
		
		apply(moneyRolledbackEvent);
		
	}
	
	
	@EventSourcingHandler
	public void on(MoneyRolledbackEvent moneyRolledbackEvent) {
		this.balance = this.balance.add(moneyRolledbackEvent.getAmount());
		LOGGER.info("MoneyRolledbackEvent in  productAggregateId: " + this.accountId +
				" and the quantity is "+ this.balance
		);
	}
	
	
	@EventSourcingHandler
	public void on(AccountCreatedEvent accountCreatedEvent) {
		this.accountId = accountCreatedEvent.getAccountId();

		this.balance = accountCreatedEvent.getBalance();
	}
	
	
	@EventSourcingHandler
	public void on(MoneyReservedEvent moneyReservedEvent) {


		this.balance = this.balance.subtract(moneyReservedEvent.getAmount());
		LOGGER.info("MoneyReservedEvent in  accountAggregateId: " + this.accountId
				+ "and the balance is "+ this.balance
		);
	}

	@EventSourcingHandler
	public void on(MoneyDepositedEvent moneyDepositedEvent) {


		this.balance = this.balance.add(moneyDepositedEvent.getAmount());
		LOGGER.info("MoneyDepositedEvent in  accountAggregateId: " + this.accountId +
				" and the balance is "+ this.balance
				);
	}

	@EventSourcingHandler
	public void on(AccountBlockedEvent ev){

		LOGGER.info("Account "+ this.accountId + "is blocked!");
		this.isBlocked = true;
	}
	

}
