package com.appsdeveloperblog.estore.AccountsService.query;

import java.util.ArrayList;
import java.util.List;

import org.axonframework.queryhandling.QueryHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.appsdeveloperblog.estore.AccountsService.core.data.AccountEntity;
import com.appsdeveloperblog.estore.AccountsService.core.data.AccountsRepository;
import com.appsdeveloperblog.estore.AccountsService.query.rest.AccountRestModel;

@Component
public class AccountsQueryHandler {
	
	private final AccountsRepository accountsRepository;
	
	public AccountsQueryHandler(AccountsRepository accountsRepository) {
		this.accountsRepository = accountsRepository;
	}
	
	@QueryHandler
	public List<AccountRestModel> findProducts(FindAccountsQuery query) {
		
		List<AccountRestModel> accountsRest = new ArrayList<>();
		
		List<AccountEntity> accounts =  accountsRepository.findAll();
		
		for(AccountEntity accountEntity : accounts) {
			AccountRestModel accountRestModel = new AccountRestModel();
			BeanUtils.copyProperties(accountEntity, accountRestModel);
			accountsRest.add(accountRestModel);
		}
		
		return accountsRest;
		
	}

}
