package com.appsdeveloperblog.estore.AccountsService.command.rest;

import java.math.BigDecimal;
import java.util.UUID;

import javax.validation.Valid;

import com.appsdeveloperblog.estore.AccountsService.command.CreateAccountCommand;
import com.appsdeveloperblog.estore.AccountsService.core.data.AccountService;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/accounts") // http://localhost:8080/accounts
public class AccountsCommandController {

	private final Environment env;
	private final CommandGateway commandGateway;
	private final AccountService accountService;


	@Autowired
	public AccountsCommandController(Environment env, CommandGateway commandGateway, AccountService accountService) {
		this.env = env;
		this.commandGateway = commandGateway;
		this.accountService = accountService;
	}

	@PostMapping
	public String createAccount(@Valid @RequestBody CreateAccountRestModel createAccountRestModel) {

		CreateAccountCommand createAccountCommand = CreateAccountCommand.builder()

				.balance(createAccountRestModel.getBalance())

				.accountId(UUID.randomUUID().toString()).build();

		String returnValue;

		returnValue = commandGateway.sendAndWait(createAccountCommand);


		return returnValue;
	}

	@PostMapping(value = "/balance")
	public ResponseEntity<?> blockAccount(@RequestBody BlockAccountRestModel order) {
		BigDecimal balance = null;

		//  deleteAxonTableRowsService.removeAllAxonRows();
		balance = accountService.getBalanceById(order.getAccountId());
		if (balance.compareTo(BigDecimal.ZERO) == 1) {
			return new ResponseEntity<>(
					balance,
					HttpStatus.OK);
		} else {
			return new ResponseEntity<>(
					"No such account exists!",
					HttpStatus.EXPECTATION_FAILED);
		}


	}
}
