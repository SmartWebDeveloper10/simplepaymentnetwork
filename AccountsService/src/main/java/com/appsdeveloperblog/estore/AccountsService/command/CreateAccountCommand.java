package com.appsdeveloperblog.estore.AccountsService.command;

import java.math.BigDecimal;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CreateAccountCommand {
	
	@TargetAggregateIdentifier
	private final String accountId;
//	private final String title;
//	private final BigDecimal price;
	private final BigDecimal balance;
	
}
