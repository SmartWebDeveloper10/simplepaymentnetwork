package com.appsdeveloperblog.estore.AccountsService.command.rest;


import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class BlockAccountRestModel {

    @NotBlank(message="Account Id is a required field")
    private String accountId;
}
