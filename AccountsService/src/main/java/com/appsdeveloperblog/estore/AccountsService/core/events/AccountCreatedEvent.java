package com.appsdeveloperblog.estore.AccountsService.core.events;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class AccountCreatedEvent {

	private String accountId;
//	private String title;
//	private BigDecimal price;
	private BigDecimal balance;
	
}
