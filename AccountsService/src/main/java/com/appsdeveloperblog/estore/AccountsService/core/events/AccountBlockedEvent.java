package com.appsdeveloperblog.estore.AccountsService.core.events;

import lombok.Data;

@Data
public class AccountBlockedEvent {

    private String accountId;
}
