package com.appsdeveloperblog.estore.AccountsService.query;

import com.appsdeveloperblog.estore.AccountsService.core.data.AccountNode;
import com.appsdeveloperblog.estore.AccountsService.core.data.AccountNodeRepository;
import com.appsdeveloperblog.estore.AccountsService.core.events.AccountBlockedEvent;
import com.appsdeveloperblog.estore.AccountsService.core.events.AccountCreatedEvent;
import com.appsdeveloperblog.estore.core.events.MoneyDepositedEvent;
import com.appsdeveloperblog.estore.core.events.MoneyRolledbackEvent;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.eventhandling.ResetHandler;
import org.axonframework.messaging.interceptors.ExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import com.appsdeveloperblog.estore.AccountsService.core.data.AccountEntity;
import com.appsdeveloperblog.estore.AccountsService.core.data.AccountsRepository;
import com.appsdeveloperblog.estore.core.events.MoneyReservedEvent;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Component
@ProcessingGroup("account-group")
public class AccountEventsHandler {

	private final AccountsRepository accountsRepository;
	private final AccountNodeRepository accountNodeRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(AccountEventsHandler.class);

	public AccountEventsHandler(AccountsRepository accountsRepository, AccountNodeRepository accountNodeRepository) {
		this.accountsRepository = accountsRepository;
		this.accountNodeRepository = accountNodeRepository;
	}
	
	@ExceptionHandler(resultType=Exception.class)
	public void handle(Exception exception) throws Exception {
		throw exception;
	}
	
	@ExceptionHandler(resultType=IllegalArgumentException.class)
	public void handle(IllegalArgumentException exception) {
		// Log error message
	}

	@EventHandler
	@Transactional
	public void on(AccountBlockedEvent ev){
		AccountEntity accountEntity = accountsRepository.findByAccountId(ev.getAccountId());
		accountEntity.setIsBlocked(true);

		AccountNode accountNode = new AccountNode();
		accountNode.setAccountId(accountEntity.getAccountId());
		accountNode.setBlocked(true);
		try {
			accountsRepository.saveAndFlush(accountEntity);
			accountNodeRepository.save(accountNode);

		} catch (IllegalArgumentException ex) {
			ex.printStackTrace();
		}


	}

	@EventHandler
	public void on(AccountCreatedEvent event) {

		AccountEntity accountEntity = new AccountEntity();
		BeanUtils.copyProperties(event, accountEntity);

		AccountNode accountNode = new AccountNode();
		accountNode.setAccountId(accountEntity.getAccountId());
		accountNode.setBlocked(false);

		try {
			accountsRepository.saveAndFlush(accountEntity);
			accountNodeRepository.save(accountNode);

		} catch (IllegalArgumentException ex) {
			ex.printStackTrace();
		}

	}

	@EventHandler
	@Transactional
	public void on(MoneyDepositedEvent productReservedEvent){
	//	LOGGER.info("MoneyDepositedEvent: Current account balance " + productReservedEvent.getAmount());
		AccountEntity accountEntity = accountsRepository.findByAccountId(productReservedEvent.getAccountToId());

		LOGGER.info("MoneyDepositedEvent: before add amount " + productReservedEvent.getAmount());

		accountEntity.setBalance(accountEntity.getBalance().add(productReservedEvent.getAmount()));

		LOGGER.info("MoneyDepositedEvent: accountId: " + productReservedEvent.getAccountToId() +
				", Amount:" + accountEntity.getBalance());
		try {
			accountsRepository.saveAndFlush(accountEntity);
		} catch (IllegalArgumentException ex) {
			ex.printStackTrace();
		}
	}

	@EventHandler
	@Transactional
	public void on(MoneyReservedEvent moneyReservedEvent) {
		AccountEntity accountEntity = accountsRepository.findByAccountId(moneyReservedEvent.getAccountFromId());
		
		LOGGER.info("MoneyReservedEvent: before subtract product quantity " + accountEntity.getBalance());
		
		accountEntity.setBalance(accountEntity.getBalance().subtract(moneyReservedEvent.getAmount()));


		try {
			accountsRepository.saveAndFlush(accountEntity);
		} catch (IllegalArgumentException ex) {
			ex.printStackTrace();
		}
		

		LOGGER.info("MoneyReservedEvent is called for productId:" + moneyReservedEvent.getAccountFromId() +
				" and Quantity : " + (accountEntity.getBalance()));
	}
	
	@EventHandler
	@Transactional
	public void on(MoneyRolledbackEvent moneyRolledbackEvent) {
		AccountEntity currentlyStoredAccount =  accountsRepository.findByAccountId(moneyRolledbackEvent.getAccountId());
	
		LOGGER.info("MoneyRolledbackEvent: Current product quantity "
		+ currentlyStoredAccount.getBalance() );
		
		BigDecimal newQuantity = currentlyStoredAccount.getBalance().add(moneyRolledbackEvent.getAmount());
		currentlyStoredAccount.setBalance(newQuantity);

		try {
			accountsRepository.saveAndFlush(currentlyStoredAccount);
		} catch (IllegalArgumentException ex) {
			ex.printStackTrace();
		}
		
		LOGGER.info("MoneyRolledbackEvent: New product quantity "
		+ currentlyStoredAccount.getBalance()+ " , product id is "+currentlyStoredAccount.getAccountId() );
	
	}
	
	@ResetHandler
	public void reset() {
		accountsRepository.deleteAll();
	}

}
