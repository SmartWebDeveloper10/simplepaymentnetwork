#### Prerequisite
- Java 11
- Axon server 4.5.7 (Download and install an axon server; then cd Axonserver-4.5.7; then java -jar axonserver.jar
- Postgres 10+ (run the docker-compose.yml in AccountService-main folder) 
- Neo4j 4.3.5 (run the script from https://hub.docker.com/_/neo4j)
#### Discovery service (Eureka)
- Build/Run
  - mvn clean install
  - java -jar target/DiscoveryServer-0.0.1-SNAPSHOT.jar
- Check
  - http://localhost:8761/
#### Api Gateway
- Build/Run
  - mvn clean install
  - java -jar target/ApiGateway-0.0.1-SNAPSHOT.jar

#### Core service (a library jar with classes, common for the Account and Transaction services)
- Build
  - mvn clean install
  
#### Account service 
- Build/Run
  - mvn clean install
  - java -jar target/AccountsService-0.0.1-SNAPSHOT.jar

#### Transactions service 
- Build/Run
  - mvn clean install
  - java -jar target/TransactionsService-0.0.1-SNAPSHOT.jar


