/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appsdeveloperblog.estore.TransactionsService.core.model;

public enum TransactionStatus {
    CREATED, APPROVED, REJECTED
}