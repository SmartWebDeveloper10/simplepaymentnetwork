package com.appsdeveloperblog.estore.TransactionsService.interceptors;


import com.appsdeveloperblog.estore.TransactionsService.command.commands.CreateTransactionCommand;
import com.appsdeveloperblog.estore.TransactionsService.query.NeoPersistenceServiceImpl;
import org.axonframework.commandhandling.CommandMessage;
import org.axonframework.messaging.MessageDispatchInterceptor;
import org.neo4j.driver.*;
import org.neo4j.driver.exceptions.NoSuchRecordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.BiFunction;

@Component
public class WithdrawMoneyCommandInterceptor implements MessageDispatchInterceptor<CommandMessage<?>> {

	private String uri;
	private String user;
	private String password;

	private final Driver driver;

	@Autowired
	private NeoPersistenceServiceImpl neoService;

	public WithdrawMoneyCommandInterceptor(@Value("${spring.neo4j.uri}") String url,
									 @Value("${spring.neo4j.authentication.username}") String user,
									 @Value("${spring.neo4j.authentication.password}") String password
	) {
		this.uri = url;
		this.user = user;
		this.password = password;

		driver = GraphDatabase.driver( uri, AuthTokens.basic( user, password ) );
	}


 
	@Override
	public BiFunction<Integer, CommandMessage<?>, CommandMessage<?>> handle(
			List<? extends CommandMessage<?>> messages) {
		 
		return (index, command) -> {
			
		//	LOGGER.info("Intercepted command: " + command.getPayloadType());
			
//			if(CreateTransactionCommand.class.equals(command.getPayloadType())) {
//
//				CreateTransactionCommand createProductCommand = (CreateTransactionCommand)command.getPayload();
//
//				try {
//					String isBlocked = neoService.isBlocked(createProductCommand.getProductFromId());
//
//					System.out.println(isBlocked);
//					if (isBlocked.equals("true")) {
//
//
//
////						Map<String,?> mData = command.getMetaData();
////						mData.put("Blocked",null);
////						command.withMetaData(mData);
////						return command;
//						throw new AccountBlockedException(
//								String.format("Transaction with userFromId %s and id %s is blocked by the police",
//										createProductCommand.getProductFromId(), createProductCommand.getOrderId())
//						);
//					}
//				}
//				catch(NoSuchRecordException e){
//					return command;
//				}
//
//			}
			
			return command;
		};
	}



}
