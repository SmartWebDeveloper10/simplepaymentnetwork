package com.appsdeveloperblog.estore.TransactionsService.query;

import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;

import com.appsdeveloperblog.estore.TransactionsService.core.data.TransactionEntity;
import com.appsdeveloperblog.estore.TransactionsService.core.data.TransactionsRepository;
import com.appsdeveloperblog.estore.TransactionsService.core.model.TransactionSummary;

@Component
public class TransactionQueriesHandler {

	TransactionsRepository transactionsRepository;

	public TransactionQueriesHandler(TransactionsRepository transactionsRepository) {
		this.transactionsRepository = transactionsRepository;
	}

	@QueryHandler
	public TransactionSummary findOrder(FindTransactionQuery findTransactionQuery) {
		TransactionEntity transactionEntity = transactionsRepository.findByTransactionId(findTransactionQuery.getTransactionId());
		return new TransactionSummary(transactionEntity.getTransactionId(),
				transactionEntity.getTransactionStatus(), "");
	}



}
