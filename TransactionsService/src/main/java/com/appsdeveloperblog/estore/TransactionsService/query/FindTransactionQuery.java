package com.appsdeveloperblog.estore.TransactionsService.query;

import lombok.Value;

@Value
public class FindTransactionQuery {

	private final String transactionId;
	
}
