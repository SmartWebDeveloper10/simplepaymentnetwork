/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appsdeveloperblog.estore.TransactionsService.command.rest;

import com.appsdeveloperblog.estore.TransactionsService.core.model.TransactionStatus;
import com.appsdeveloperblog.estore.TransactionsService.core.model.TransactionSummary;
import com.appsdeveloperblog.estore.TransactionsService.interceptors.AccountBlockedException;
import com.appsdeveloperblog.estore.TransactionsService.query.FindTransactionQuery;
import com.appsdeveloperblog.estore.TransactionsService.command.commands.CreateTransactionCommand;
import java.util.UUID;
import javax.validation.Valid;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.axonframework.queryhandling.SubscriptionQueryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transactions")
public class TransactionsCommandController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TransactionsCommandController.class);
	private final CommandGateway commandGateway;
	private final QueryGateway queryGateway;


	@Autowired
	public TransactionsCommandController(CommandGateway commandGateway, QueryGateway queryGateway) {
		this.commandGateway = commandGateway;
		this.queryGateway = queryGateway;
	}

	@PostMapping
	public ResponseEntity<String> createOrder(@Valid @RequestBody TransactionCreateRest order) {


		String transactionId = UUID.randomUUID().toString();

		CreateTransactionCommand createTransactionCommand = CreateTransactionCommand.builder()
				.accountToId(order.getAccountToId())
				.accountFromId(order.getAccountFromId())
				.amount(order.getAmount())

				.transactionId(transactionId)
				.transactionStatus(TransactionStatus.CREATED).build();

		SubscriptionQueryResult<TransactionSummary, TransactionSummary> queryResult = queryGateway.subscriptionQuery(
				new FindTransactionQuery(transactionId), ResponseTypes.instanceOf(TransactionSummary.class),
				ResponseTypes.instanceOf(TransactionSummary.class));

		try {
			String res = commandGateway.sendAndWait(createTransactionCommand);
			LOGGER.info("commandGateway.sendAndWait(createTransactionCommand) " + res);
			ObjectMapper mapper = new ObjectMapper();
			String resp = mapper.writeValueAsString(queryResult.updates().blockFirst());
			if(resp.length()>=1) {
				LOGGER.info("about to send RespEntity with ", resp);
				return new ResponseEntity<>(resp, HttpStatus.OK);
			}
			else{
				return new ResponseEntity<>("Oups! Something went wrong!", HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		catch(AccountBlockedException e){LOGGER.info("Transaction Command Controller Error  " + e.getMessage());
			return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
		}
		catch(JsonProcessingException e){
			return new ResponseEntity<>("Oups! Something went wrong!", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		 finally {
			queryResult.close();
		}

	}

}
