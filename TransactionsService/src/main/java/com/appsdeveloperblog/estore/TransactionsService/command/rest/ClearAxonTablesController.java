package com.appsdeveloperblog.estore.TransactionsService.command.rest;




import com.appsdeveloperblog.estore.TransactionsService.core.data.TransactionsRepository;
import com.appsdeveloperblog.estore.TransactionsService.query.NeoPersistenceServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/clean-transactions-axon")
public class ClearAxonTablesController {

private static final Logger LOGGER = LoggerFactory.getLogger(ClearAxonTablesController.class);
    @Autowired
    private TransactionsRepository transactionsRepository;

    @Autowired
    private NeoPersistenceServiceImpl neoService;

    @DeleteMapping
    public ResponseEntity<String> createProduct(){

        try {

            transactionsRepository.deleteAll();
            transactionsRepository.deleteAllSagaTables();
            transactionsRepository.deleteAllAssociateTables();
            transactionsRepository.deleteAllTokenTables();
            return new ResponseEntity<>(
                    "Aux Transactions Axon tables cleaned",
                    HttpStatus.OK);
        }
        catch(Exception e){
            return new ResponseEntity<>(
                    e.getMessage(),
                    HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PostMapping(value = "/block")
    public ResponseEntity<String> blockAccount(@RequestBody TransactionCreateRest order){
        try{
            String  result = neoService.blockAccount(order.getAccountFromId());
            LOGGER.info("/blocked output: ",result);
            return new ResponseEntity<>(
                    "Account "+ order.getAccountFromId() + " is blocked!",
                    HttpStatus.OK);
        }
        catch (Exception e){
            LOGGER.info("/blocked output exception: ",e.getMessage());
            return new ResponseEntity<>(
                    e.getMessage(),
                    HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PostMapping(value = "/connect")
    public ResponseEntity<String> connectAccounts(@RequestBody TransactionCreateRest order){
        try{
            String  result = neoService.connectAccounts(order.getRelation(), order.getAccountFromId(), order.getAccountToId());
            LOGGER.info("/connect output: ",result);
            return new ResponseEntity<>(
                    "Accounts "+ order.getAccountFromId() + " and "+order.getAccountToId() + " are connected as "+ result,
                    HttpStatus.OK);
        }
        catch (Exception e){
            LOGGER.info("/connect output exception: ",e.getMessage());
            return new ResponseEntity<>(
                    e.getMessage(),
                    HttpStatus.EXPECTATION_FAILED);
        }
    }
    @PostMapping(value = "/unblock")
    public ResponseEntity<String> unblockAccount(@RequestBody TransactionCreateRest order){
        try{
            neoService.unblockAccount(order.getAccountFromId());
            return new ResponseEntity<>(
                    "Account "+ order.getAccountFromId() + " is unblocked!",
                    HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(
                    e.getMessage(),
                    HttpStatus.EXPECTATION_FAILED);
        }
    }
}
