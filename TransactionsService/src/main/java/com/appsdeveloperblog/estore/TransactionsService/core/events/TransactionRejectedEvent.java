package com.appsdeveloperblog.estore.TransactionsService.core.events;

import com.appsdeveloperblog.estore.TransactionsService.core.model.TransactionStatus;

import lombok.Value;

@Value
public class TransactionRejectedEvent {
	private final String transactionId;
	private final String reason;
	private final TransactionStatus transactionStatus = TransactionStatus.REJECTED;
}
