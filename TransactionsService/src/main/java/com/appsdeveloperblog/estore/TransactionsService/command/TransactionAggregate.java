/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appsdeveloperblog.estore.TransactionsService.command;

import com.appsdeveloperblog.estore.TransactionsService.command.commands.ApproveTransactionCommand;
import com.appsdeveloperblog.estore.TransactionsService.core.events.TransactionApprovedEvent;
import com.appsdeveloperblog.estore.TransactionsService.core.events.TransactionCreatedEvent;
import com.appsdeveloperblog.estore.TransactionsService.core.events.TransactionRejectedEvent;
import com.appsdeveloperblog.estore.TransactionsService.core.model.TransactionStatus;
import com.appsdeveloperblog.estore.TransactionsService.command.commands.CreateTransactionCommand;
import com.appsdeveloperblog.estore.TransactionsService.command.commands.RejectTransactionCommand;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventhandling.Timestamp;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.messaging.MetaData;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;

@Aggregate
public class TransactionAggregate {

    @AggregateIdentifier
    private String transactionId;
    private String accountToId;
    private String accountFromId;

    private BigDecimal amount;

     private TransactionStatus transactionStatus;
    
    public TransactionAggregate() {
    }

    @CommandHandler
    public TransactionAggregate(CreateTransactionCommand createTransactionCommand, MetaData mData) {
        TransactionCreatedEvent transactionCreatedEvent = new TransactionCreatedEvent();
        BeanUtils.copyProperties(createTransactionCommand, transactionCreatedEvent);

        if(createTransactionCommand.getTransactionStatus().equals(TransactionStatus.REJECTED)){
            TransactionRejectedEvent transactionRejectedEvent = new TransactionRejectedEvent(createTransactionCommand.getTransactionId(),"Blocked");
            AggregateLifecycle.apply(transactionRejectedEvent);
        }
        else {
            AggregateLifecycle.apply(transactionCreatedEvent);
        }
    }

    @EventSourcingHandler
    public void on(TransactionCreatedEvent transactionCreatedEvent) throws Exception {
        this.transactionId = transactionCreatedEvent.getTransactionId();
        this.accountToId = transactionCreatedEvent.getAccountToId();
        this.accountFromId = transactionCreatedEvent.getAccountFromId();

        this.amount = transactionCreatedEvent.getAmount();

        this.transactionStatus =  transactionCreatedEvent.getTransactionStatus();

    }
    
    @CommandHandler
    public void handle(ApproveTransactionCommand approveTransactionCommand) {
    	// Create and publish the TransactionApprovedEvent
    	
    	TransactionApprovedEvent transactionApprovedEvent =
                TransactionApprovedEvent.builder()
                        .transactionId(approveTransactionCommand.getTransactionId())
                        .accountFromId(approveTransactionCommand.getAccountFromId())
                        .accountToId(approveTransactionCommand.getAccountToId())
                        .build();


    	
    	AggregateLifecycle.apply(transactionApprovedEvent);
    }
    
    @EventSourcingHandler
    public void on(TransactionApprovedEvent transactionApprovedEvent) {
    	this.transactionStatus = transactionApprovedEvent.getTransactionStatus();

    }
 
    @CommandHandler
    public void handle(RejectTransactionCommand rejectTransactionCommand) {
    	
    	TransactionRejectedEvent transactionRejectedEvent = new TransactionRejectedEvent(rejectTransactionCommand.getTransactionId(),
    			rejectTransactionCommand.getReason());
    	
    	AggregateLifecycle.apply(transactionRejectedEvent);
    	
    }
    
    @EventSourcingHandler
    public void on(TransactionRejectedEvent transactionRejectedEvent) {
    	this.transactionStatus = transactionRejectedEvent.getTransactionStatus();

    }
    

}
