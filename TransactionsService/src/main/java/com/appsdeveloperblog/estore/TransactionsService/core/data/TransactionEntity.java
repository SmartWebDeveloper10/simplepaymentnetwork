/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appsdeveloperblog.estore.TransactionsService.core.data;

import com.appsdeveloperblog.estore.TransactionsService.core.model.TransactionStatus;
import java.io.Serializable;
import java.math.BigDecimal;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import javax.persistence.*;


import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.repository.Temporal;

@Data
@Entity
@Table(name = "transactions")
public class TransactionEntity implements Serializable {

    private static final long serialVersionUID = 5313493413859894403L;

    @Id
    @Column(unique = true)
    public String transactionId;
    private String accountToId;
    private String accountFromId;
  //  private String userId;
    private BigDecimal amount;
  //  private String addressId;


    @Enumerated(EnumType.STRING)
    private TransactionStatus transactionStatus;

  //  @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false)
    @CreationTimestamp
    private LocalDateTime created;

   // @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false)
    @UpdateTimestamp
    private LocalDateTime updated;

//    @PrePersist
//    protected void onCreate() {
//        updated = created = new Date();
//    }
//
//    @PreUpdate
//    protected void onUpdate() {
//        updated = new Date();
//    }
}
