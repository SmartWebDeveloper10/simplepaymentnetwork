package com.appsdeveloperblog.estore.TransactionsService.core.events;

import com.appsdeveloperblog.estore.TransactionsService.core.model.TransactionStatus;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class TransactionApprovedEvent {

	private final String transactionId;
	private final TransactionStatus transactionStatus = TransactionStatus.APPROVED;
	private final String accountToId;
	private final String accountFromId;
	
}
