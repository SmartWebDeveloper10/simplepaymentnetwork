package com.appsdeveloperblog.estore.TransactionsService.query;


import org.neo4j.driver.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class NeoPersistenceServiceImpl {


    private String uri;
    private String user;
    private String password;

    private final Driver driver;

    public NeoPersistenceServiceImpl(@Value("${spring.neo4j.uri}") String url,
                                     @Value("${spring.neo4j.authentication.username}") String user,
                                     @Value("${spring.neo4j.authentication.password}") String password
    ) {
        this.uri = url;
        this.user = user;
        this.password = password;

        driver = GraphDatabase.driver( uri, AuthTokens.basic( user, password ) );
    }

    public String persistOrderRelation(String transactionId, String accountFromId, String accountToId){
        Map<String, Object> params = new HashMap<>();
        params.put("transactionId",transactionId);
        params.put("accountFromId",accountFromId);
        params.put("accountToId",accountToId);

        try ( Session session = driver.session() )
        {
            String greeting = session.writeTransaction( new TransactionWork<String>()
            {
                @Override
                public String execute( Transaction tx )
                {
                    Result result = tx.run(
                            "MERGE (aFrom: Account {accountId: $accountFromId}) "+
                                    " MERGE (aTo: Account {accountId: $accountToId}) "+
                                    " MERGE (aFrom) -[l: Transaction] ->(aTo) " +
                                            " ON CREATE SET l.transactionId =  $transactionId "+
                                              " ON MATCH SET l.transactionId =  $transactionId "+
                                    "RETURN l.transactionId",
                            params );
                    return result.single().get( 0 ).asString();
                }
            } );

            return greeting;
        }
    }

    public String connectAccounts(String relationType, String accountFromId, String accountToId){
        Map<String, Object> params = new HashMap<>();
        params.put("relationType",relationType);
        params.put("accountFromId",accountFromId);
        params.put("accountToId",accountToId);

        try ( Session session = driver.session() )
        {
            String greeting = session.writeTransaction( new TransactionWork<String>()
            {
                @Override
                public String execute( Transaction tx )
                {
                    Result result = tx.run(
                            "MERGE (aFrom: Account {accountId: $accountFromId}) "+
                                    " MERGE (aTo: Account {accountId: $accountToId}) "+
                                    " MERGE (aFrom) -[r: Relation] ->(aTo) " +
                                    " ON CREATE SET r.relationType =  $relationType "+
                                    " ON MATCH SET r.relationType =  $relationType "+
                                    "RETURN r.relationType",
                            params );
                    return result.single().get( 0 ).asString();
                }
            } );

            return greeting;
        }
    }

    public String isBlocked( String accountFromId){
        Map<String, Object> params = new HashMap<>();

        params.put("accountFromId",accountFromId);


        try ( Session session = driver.session() )
        {
            String greeting = session.writeTransaction( new TransactionWork<String>()
            {
                @Override
                public String execute(Transaction tx )
                {
                    Result result = tx.run(
                            "MATCH (aFrom: Account) "+
                                    "WHERE EXISTS(aFrom.blocked) AND aFrom.accountId=$accountFromId AND aFrom.blocked=true "+
                                    "RETURN toString(aFrom.blocked);"


                            ,
                            params );
                    return result.single().get( 0 ).asString();

                }
            } );

            return greeting;
        }
    }



    public String blockAccount( String accountFromId){
        Map<String, Object> params = new HashMap<>();

        params.put("accountFromId",accountFromId);


        try ( Session session = driver.session() )
        {
            String greeting = session.writeTransaction( new TransactionWork<String>()
            {
                @Override
                public String execute( Transaction tx )
                {
                    Result result = tx.run(
                            "MATCH (aFrom: Account {accountId: $accountFromId}) "+
                                    "SET aFrom.blocked = true " +
                                    "RETURN toString(aFrom.blocked);"
                            ,
                            params );
                    return result.single().get( 0 ).asString();
                }
            } );

            return greeting;
        }
    }

    public String unblockAccount( String accountFromId){
        Map<String, Object> params = new HashMap<>();

        params.put("accountFromId",accountFromId);


        try ( Session session = driver.session() )
        {
            String greeting = session.writeTransaction( new TransactionWork<String>()
            {
                @Override
                public String execute( Transaction tx )
                {
                    Result result = tx.run(
                            "MATCH (aFrom: Account{accountId: $accountFromId} "+
                                    "SET aFrom.blocked = false " +
                                    "RETURN toString(aFrom.blocked);",
                            params );
                    return result.single().get( 0 ).asString();
                }
            } );

            return greeting;
        }
    }
}
