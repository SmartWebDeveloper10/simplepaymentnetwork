/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appsdeveloperblog.estore.TransactionsService.query;

import com.appsdeveloperblog.estore.TransactionsService.core.data.TransactionEntity;
import com.appsdeveloperblog.estore.TransactionsService.core.data.TransactionsRepository;
import com.appsdeveloperblog.estore.TransactionsService.core.events.TransactionApprovedEvent;
import com.appsdeveloperblog.estore.TransactionsService.core.events.TransactionCreatedEvent;
import com.appsdeveloperblog.estore.TransactionsService.core.events.TransactionRejectedEvent;

import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.eventhandling.Timestamp;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
@ProcessingGroup("order-group")
public class TransactionEventsHandler {
    
    private final TransactionsRepository transactionsRepository;
    private final NeoPersistenceServiceImpl neoPersistenceService;
    
    public TransactionEventsHandler(TransactionsRepository transactionsRepository, NeoPersistenceServiceImpl neoPersistenceService) {
        this.transactionsRepository = transactionsRepository;
        this.neoPersistenceService = neoPersistenceService;
    }

    @EventHandler
    public void on(TransactionCreatedEvent event

    ) throws Exception {
        TransactionEntity transactionEntity = new TransactionEntity();

        BeanUtils.copyProperties(event, transactionEntity);


        transactionsRepository.save(transactionEntity);
    }
    
    
    @EventHandler
    public void on(TransactionApprovedEvent transactionApprovedEvent

    ) {
    	TransactionEntity transactionEntity = transactionsRepository.findByTransactionId(transactionApprovedEvent.getTransactionId());
   
    	if(transactionEntity == null) {
    		// TODO: Do something about it
    		return;
    	}
    	
    	transactionEntity.setTransactionStatus(transactionApprovedEvent.getTransactionStatus());

    	neoPersistenceService.persistOrderRelation(transactionApprovedEvent.getTransactionId()
                , transactionApprovedEvent.getAccountFromId()
                , transactionApprovedEvent.getAccountToId());
    	
    	transactionsRepository.save(transactionEntity);
    
    }
    
    @EventHandler
    public void on(TransactionRejectedEvent transactionRejectedEvent

    ) {
    	TransactionEntity transactionEntity = transactionsRepository.findByTransactionId(transactionRejectedEvent.getTransactionId());
    	transactionEntity.setTransactionStatus(transactionRejectedEvent.getTransactionStatus());

    	transactionsRepository.save(transactionEntity);
    }
    
}
