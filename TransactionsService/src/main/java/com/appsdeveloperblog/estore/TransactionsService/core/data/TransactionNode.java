package com.appsdeveloperblog.estore.TransactionsService.core.data;


import lombok.Data;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;

@Data
@Node("Order")
public class TransactionNode {
    @Id
    public String orderId;

    public String productFromId;
    public String productToId;
}
