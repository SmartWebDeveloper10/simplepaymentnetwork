package com.appsdeveloperblog.estore.TransactionsService.interceptors;

public class AccountBlockedException extends RuntimeException{
    public AccountBlockedException(String msg){
        super(msg);
    }
}
