package com.appsdeveloperblog.estore.TransactionsService.command.rest;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransactionCreateRest {

    @NotBlank(message = "AccountFromId is a required field")
    private String accountFromId;

    @NotBlank(message = "AccountToId is a required field")
    private String accountToId;
    

    @Max(value = 500, message = "Amount cannot be larger than 500")
    private BigDecimal amount;
    

    private String relation;
    
}
