/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appsdeveloperblog.estore.TransactionsService.command.commands;

import com.appsdeveloperblog.estore.TransactionsService.core.model.TransactionStatus;
import lombok.Builder;
import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.math.BigDecimal;

@Builder
@Data
public class CreateTransactionCommand {
        
    @TargetAggregateIdentifier
    public final String transactionId;

    private final String accountToId;
    private final String accountFromId;
    private final BigDecimal amount;

    private final TransactionStatus transactionStatus;
}
